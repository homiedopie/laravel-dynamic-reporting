<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function app_model()
    {
        return $this->belongsTo(\App\Model::class, 'model_id');
    }

    public function report_model_relations()
    {
        return $this->hasMany(ReportModelRelation::class);
    }

    public function filter_groups()
    {
        return $this->hasMany(FilterGroup::class);
    }
}

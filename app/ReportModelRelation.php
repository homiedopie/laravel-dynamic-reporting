<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportModelRelation extends Model
{
    public function report()
    {
        return $this->belongsTo(Report::class);
    }

    public function model_relation()
    {
        return $this->belongsTo(\App\ModelRelation::class);
    }
}

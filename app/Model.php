<?php

namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    public function model_relations()
    {
        return $this->hasMany(ModelRelation::class);
    }

    public function metas()
    {
        return $this->hasMany(Meta::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}

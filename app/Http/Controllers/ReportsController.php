<?php

namespace App\Http\Controllers;

use App\Report;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function show($id = null)
    {
        $report = Report::with([
            'report_model_relations',
            'report_model_relations.model_relation',
            'app_model',
            'app_model.model_relations',
            'filter_groups' => function($query) {
                $query->orderBy('order', 'ASC');
            },
            'filter_groups.filters' => function($query) {
                $query->orderBy('order', 'ASC');
            },
            'filter_groups.filters.meta'
        ])->where('id', $id)->first();

        $model = $report->app_model->name;
        $model_relations = $report->app_model->model_relations;
        $model_relations_check = [];
        foreach ($model_relations as $relation) {
            $model_relations_check[$relation->relation_model_id] = [
                'key' => $relation->relation_key
            ];
        }

        $report_query = $model::where(function($query) use ($report, $model_relations_check) {
            $filter_groups = $report->filter_groups;
            $model_id = $report->app_model->id;
            foreach ($filter_groups as $group) {
                $filters = $group->filters;
                $filter_group_query = function ($query) use ($filters, $model_id, $model_relations_check) {
                    foreach ($filters as $filter) {
                        $filter_value = $filter->value;
                        $filter_meta = $filter->meta;
                        $filter_model_id = $filter_meta->model_id;
                        $filter_is_relation = $model_id != $filter_model_id;
                        $filter_meta_column = $filter_meta->name;
                        $filter_condition = $filter->condition;
                        $filter_join_condition = $filter->join_condition;
                        $filter_query_condition = ($filter_join_condition == 'or' ? 'orWhere' : 'where');

                        //TODO: Refactor
                        if (!$filter_is_relation) {
                            switch ($filter_condition) {
                                case 'ends_with':
                                    $query->$filter_query_condition($filter_meta_column, 'like', '%'.$filter_value);
                                    break;
                                case 'date':
                                    if ($filter_value == 'current_year') {
                                        $query->$filter_query_condition(function($query) use ($filter_meta_column){
                                            $query->whereBetween($filter_meta_column, [Carbon::now()->startOfYear(), Carbon::now()->endOfYear()]);
                                        });
                                    }
                                    break;
                            }
                        } else {
                            $relation_query_condition = $filter_query_condition.'Has';
                            $relation_key = isset($model_relations_check[$filter_model_id]) ? $model_relations_check[$filter_model_id]['key'] : null;
                            if ($relation_key) {
                                $query->$relation_query_condition($relation_key, function ($query) use ($filter) {
                                    $filter_value = $filter->value;
                                    $filter_meta = $filter->meta;
                                    $filter_meta_column = $filter_meta->name;
                                    $filter_condition = $filter->condition;
                                    $filter_join_condition = $filter->join_condition;
                                    $filter_query_condition = ($filter_join_condition == 'or' ? 'orWhere' : 'where');

                                    switch ($filter_condition) {
                                        case 'ends_with':
                                            $query->$filter_query_condition($filter_meta_column, 'like', '%'.$filter_value);
                                            break;
                                        case 'date':
                                            if ($filter_value == 'current_year') {
                                                $query->$filter_query_condition(function($query) use ($filter_meta_column){
                                                    $query->whereBetween($filter_meta_column, [Carbon::now()->startOfYear(), Carbon::now()->endOfYear()]);
                                                });
                                            }
                                            break;
                                    }
                                });
                            }
                        }
                    }
                };

                $join_condition = $group->join_condition == 'or' ? 'orWhere' : 'where';
                $query->$join_condition($filter_group_query);
            }
        });

        $report_model_relations = $report->report_model_relations;
        if ($report_model_relations) {
            $report_relations = [];
            foreach ($report_model_relations as $relation) {
                if (isset($model_relations_check[$relation->model_relation->relation_model_id])) {
                    $report_relations[] = $model_relations_check[$relation->model_relation->relation_model_id]['key'];
                }
            }
            $report_query->with($report_relations);
        }

        return $report_query->get()->toArray();
    }
}

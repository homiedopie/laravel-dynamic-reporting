<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterGroup extends Model
{
    //
    public function filters()
    {
        return $this->hasMany(Filter::class);
    }
}

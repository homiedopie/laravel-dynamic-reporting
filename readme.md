## Laravel Dynamic Reporting - Practical exam
- This is still work in progress and needs to be refactored

## Installation:
1. Do *git@bitbucket.org:homiedopie/laravel-dynamic-reporting.git*
2. Run *composer update --no-script*
3. Run *npm install* or *npm install --no-bin-links* for Windows Host OS
4. Run cp .env.example .env 
5. Setup the Environment Variables (Additional config items are GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET)
7. Run the migration *php artisan migrate --seed*
9. Run *npm run dev* or *npm run production* to compile the assets
10. Access the app in the browser (/reports/{id})
11. Register and login (Manual registration or via Google)
12. Have fun!
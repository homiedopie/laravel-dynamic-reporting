<?php

use Illuminate\Database\Seeder;

class SampleModelMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_model = \App\Model::create([
            'name' => \App\User::class,
        ]);

        $website_model = \App\Model::create([
            'name' => \App\Website::class,
        ]);

        $user_model->model_relations()->create([
            'relation_model_id' => $website_model->id,
            'relation' => 'has_many',
            'relation_key' => 'websites',
        ]);

        $user_model->metas()->createMany([
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => 'Created',
                'type' => 'date'
            ]
        ]);

        $website_model->model_relations()->create([
            'relation_model_id' => $user_model->id,
            'relation' => 'belongs_to',
            'relation_key' => 'user',
        ]);

        $website_model->metas()->createMany([
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => 'Created',
                'type' => 'date'
            ]
        ]);
    }
}

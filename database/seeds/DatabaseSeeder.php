<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserWebsiteSeeder::class);
        $this->call(SampleModelMetaSeeder::class);
        $this->call(SampleReportFilterSeeder::class);
    }
}

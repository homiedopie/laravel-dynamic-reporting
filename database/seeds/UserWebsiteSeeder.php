<?php

use Illuminate\Database\Seeder;

class UserWebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 50)->create()->each(function ($u) {
            $u->websites()->saveMany(factory(App\Website::class, 100)->times(100)->make());
        });
    }
}

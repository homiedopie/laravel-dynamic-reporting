<?php

use Illuminate\Database\Seeder;

class SampleReportFilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $website_model = \App\Model::where('name', \App\Website::class)->first();
        $user_model = \App\Model::where('name', \App\User::class)->first();

        $report = $website_model->reports()->create([
            'title' => 'Sample Report'
        ]);

        $website_year_meta = \App\Meta::where('model_id', $website_model->id)->where('name', 'created_at')->first();
        $website_name_meta = \App\Meta::where('model_id', $website_model->id)->where('name', 'name')->first();

        $user_name_meta = \App\Meta::where('model_id', $user_model->id)->where('name', 'name')->first();

        $filter_group_1 = $report->filter_groups()->create([
            'join_condition' => null,
            'order' => 0
        ]);

        $filter_group_1->filters()->createMany([
            [
                'meta_id' => $website_year_meta->id,
                'condition' => 'date',
                'value' => 'current_year',
                'join_condition' => null,
                'order' => 0,
            ],
            [
                'meta_id' => $website_name_meta->id,
                'condition' => 'ends_with',
                'value' => '.net',
                'join_condition' => 'and',
                'order' => 1
            ]
        ]);

        $filter_group_2 = $report->filter_groups()->create([
            'join_condition' => 'or',
            'order' => 1
        ]);

        $filter_group_2->filters()->createMany([
            [
                'meta_id' => $website_name_meta->id,
                'condition' => 'ends_with',
                'value' => '.com',
                'join_condition' => 'or',
                'order' => 0,
            ],
        ]);

        $report_2 = $website_model->reports()->create([
            'title' => 'Test Report Join'
        ]);

        $model_relation = \App\ModelRelation::where('model_id', $website_model->id)->where('relation_model_id', $user_model->id)->first();

        $report_model_relation = $report_2->report_model_relations()->create([
            'model_relation_id' => $model_relation->id,
        ]);

        $filter_2_group_1 = $report_2->filter_groups()->create([
            'join_condition' => null,
            'order' => 0
        ]);
        $sample_user = \App\User::first();
        $filter_2_group_1->filters()->createMany([
            [
                'meta_id' => $user_name_meta->id,
                'condition' => 'ends_with',
                'value' => $sample_user->name,
                'join_condition' => 'and',
                'order' => 1
            ]
        ]);

        $report_3 = $user_model->reports()->create([
            'title' => 'Test Report Join User+Website'
        ]);
        $model_relation = \App\ModelRelation::where('model_id', $user_model->id)->where('relation_model_id', $website_model->id)->first();
        $report_model_relation = $report_3->report_model_relations()->create([
            'model_relation_id' => $model_relation->id,
        ]);
        $filter_3_group_1 = $report_3->filter_groups()->create([
            'join_condition' => null,
            'order' => 0
        ]);
        $sample_user = \App\User::first();
        $filter_3_group_1->filters()->createMany([
            [
                'meta_id' => $user_name_meta->id,
                'condition' => 'ends_with',
                'value' => $sample_user->name,
                'join_condition' => 'and',
                'order' => 1
            ]
        ]);
    }
}

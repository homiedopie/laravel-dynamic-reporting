<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('model_id');
            $table->unsignedInteger('relation_model_id');
            $table->string('relation');
            $table->string('relation_key');

            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('relation_model_id')->references('id')->on('models');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_relations');
    }
}

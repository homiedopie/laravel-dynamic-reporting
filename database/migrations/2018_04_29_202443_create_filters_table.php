<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meta_id')->nullable()->default(null);
            $table->string('condition')->nullable()->default(null);
            $table->string('value')->nullable()->default(null);

            $table->string('join_condition')->nullable()->default(null);
            $table->integer('order')->nullable()->default(null);

            $table->unsignedInteger('filter_group_id')->nullable()->default(null);

            $table->foreign('meta_id')->references('id')->on('metas');
            $table->foreign('filter_group_id')->references('id')->on('filter_groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}

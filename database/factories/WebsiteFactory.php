<?php

use Faker\Generator as Faker;

$factory->define(App\Website::class, function (Faker $faker) {
    return [
        'name' => $faker->domainName,
        'created_at' => $faker->dateTimeThisDecade()
    ];
});
